﻿using CafeNextFramework.Helpers.Action;
using CafeNextFramework.TestAccess;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using DailyReport.Utilities;
using System;
using System.Threading;
using CafeNextFramework.Helpers.Verification;
using OpenQA.Selenium.Support.UI;

namespace DailyReport.Pages
{
    internal class HomePage
    {
        private readonly IWebDriver driver;
        private readonly MasterSheetRow masterSheetRow;
        private readonly WebDriverWait wait;

        public HomePage(IWebDriver driver, MasterSheetRow masterSheet)
        {
            this.driver = driver;
            masterSheetRow = masterSheet;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
            PageFactory.InitElements(driver, this);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        [FindsBy(How = How.XPath, Using = "//h1[@class='intro-title']")]
        private IWebElement errorPageHeader;

        [FindsBy(How = How.XPath, Using = "//div[@class='component component-intro']//p")]
        private IWebElement errorPageDescription;

        [FindsBy(How = How.XPath, Using = "//form[@id='loginForm']/div[@id='formsAuthenticationArea']")]
        private IWebElement loginForm;

        [FindsBy(How = How.XPath, Using = "//input[@id='userNameInput']")]
        private IWebElement userNameField;

        [FindsBy(How = How.XPath, Using = "//input[@id='passwordInput']")]
        private IWebElement passwordField;

        [FindsBy(How = How.XPath, Using = "//span[@id='submitButton']")]
        private IWebElement signInButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='form-actions']//button[@class='btn btn-primary']")]
        private IWebElement submitButton;

        [FindsBy(How = How.XPath, Using = "//button[@id='ext-gen23']")]
        private IWebElement taskLink;

        [FindsBy(How = How.XPath, Using = "//button[@id='ext-gen27']")]
        private IWebElement incidentLink;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-saved-search-name' and text()='Open Incidents with Active Tasks for My Teams']")]
        private IWebElement openIncidentsActiveTasks;
        
        [FindsBy(How = How.XPath, Using = "//div[@class='x-saved-search-name' and text()='Breached Tickets For My Team']")]
        private IWebElement breachedTicketsForMyTeam;

        [FindsBy(How = How.XPath, Using = "//div[@id='SearchController']//em[@class='x-btn-arrow']")]
        private IWebElement myOpenTask;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-saved-search-name' and text()='My Teams Open Tasks']")]
        private IWebElement myOpenTasksValue;
        
        [FindsBy(How = How.XPath, Using = "//div[@class='x-saved-search-name' and text()='All']")]
        private IWebElement allTasksValue;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-saved-search-name' and text()='My Teams Tasks']")]
        private IWebElement myTeamTasksValue;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-hd-inner x-grid3-hd-5']")]
        private IWebElement parentObjectName;

        [FindsBy(How = How.XPath, Using = "//div[text()='BreachPassed']")]
        private IWebElement breachedPassTimeName;

        [FindsBy(How = How.XPath, Using = "//div[text()='BreachPassed']/a")]
        private IWebElement breachedPassDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-form-element']//label[text()='true']/../input")]
        private IWebElement trueCheckBox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-form-element']//label[text()='false']/../input")]
        private IWebElement falseCheckBox;

        [FindsBy(How = How.XPath, Using = "//div/table/thead/tr/td/div[contains(@class,'active')]")]
        private IWebElement activeDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-hd-inner x-grid3-hd-5']/a")]
        private IWebElement parentObjectdropdown;

        [FindsBy(How = How.XPath, Using = "//li[@class='x-menu-list-item ']//a[@class='x-menu-item x-menu-item-arrow']/span[text()='Filters']")]
        private IWebElement filters;

        [FindsBy(How = How.XPath, Using = "//input[contains(@class,'x-form-text x-form-field x-grid-filter-string x-column')]")]
        private IWebElement parentObjectFilterTextbox;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'x-grid3-body')]/div[contains(@class,'x-grid3-row')]")]
        private IWebElement searchedRecords;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-panel-tbar x-panel-tbar-noheader x-panel-tbar-noborder']//td[@class='x-toolbar-left']//td[@class='x-btn-mc']/em/button[contains(@class,'x-btn-text') and text()='Export']")]
        private IWebElement exportButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='ext-comp-1556']")]
        private IWebElement errorPopup;

        [FindsBy(How = How.XPath, Using = "//button[text()='Yes']")]
        private IWebElement popupYesButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-hd-inner x-grid3-hd-2']")]
        private IWebElement status;

        [FindsBy(How = How.XPath, Using = "//div/table/thead/tr/td/div[contains(@class,'active')]")]
        private IWebElement activestatus;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-hd-inner x-grid3-hd-2']/a")]
        private IWebElement statusDropdown;
        
        [FindsBy(How = How.XPath, Using = "//a[@class='x-menu-item x-menu-item-arrow']/span[text()='Filters']")]
        private IWebElement statusFilters;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-body']/div/table/tbody/tr/td/div[text()='Accepted']/../../td/div[not(text()='Accepted')]")]
        private IWebElement acceptCkeckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-body']/div/table/tbody/tr/td/div[text()='Assigned']/../../td/div[not(text()='Assigned')]")]
        private IWebElement assignedCkeckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-body']/div/table/tbody/tr/td/div[text()='Logged']/../../td/div[not(text()='Logged')]")]
        private IWebElement loggedCkeckbox;

        [FindsBy(How = How.XPath, Using = "//label[text()='Status:']/../div/input")]
        private IWebElement clearFilterField;

        [FindsBy(How = How.XPath, Using = "//button[text()='Clear Filter']")]
        private IWebElement clearFilterButton;

        //Future breach XPaths
        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='Check/Uncheck All']/../following-sibling::td/div")]
        private IWebElement checkUncheckCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='Check/Uncheck All']/../following-sibling::td/div/div")]
        private IWebElement UncheckCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='Assigned']/../following-sibling::td/div/div")]
        private IWebElement assignedCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='In Progress']/../following-sibling::td/div/div")]
        private IWebElement inProgressCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='Logged']/../following-sibling::td/div/div")]
        private IWebElement loggedCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='Received']/../following-sibling::td/div/div")]
        private IWebElement receivedCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='Waiting for 3rd Party']/../following-sibling::td/div/div")]
        private IWebElement waitingFor3rdPartyCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='Waiting for Change']/../following-sibling::td/div/div")]
        private IWebElement waitingForChangeCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@class='x-grid3-viewport']//div[@class='x-grid3-body']//table[@class='x-grid3-row-table']//div[text()='Waiting for Problem Resolution']/../following-sibling::td/div/div")]
        private IWebElement waitingForProblemResolutionCheckbox;

        //Performing Login
        public void PerformLogin()
        {
            try
            {
                if (loginForm.Displayed)
                {
                    userNameField.SendKeys("UserName", "Login Page", "User name field", masterSheetRow);
                    passwordField.SendKeys("Password", "Login Page", "Password field", masterSheetRow);
                    signInButton.Click("Login Page", "Signin link", masterSheetRow);
                    wait.Until(ExpectedConditions.ElementToBeClickable(submitButton));
                    if (submitButton.VerifyElement(driver, "Submit button", masterSheetRow))
                    {
                        Actions action = new Actions(driver);
                        action.MoveToElement(submitButton).Build().Perform();
                        submitButton.Click("Login Page", "Submit button", masterSheetRow);
                    }
                }
                else
                {
                    masterSheetRow.TestResultLogger.Passed("Verify user logged in", "User is logged in to ServEss", "User should logged in to ServEss", "User logged in to ServEss");
                }
            }
            catch (NoSuchElementException)
            {
                masterSheetRow.TestResultLogger.Passed("Verify user loggedin", "User is logged in to ServEss", "User is already logged in", "User logged in to ServEss");
            }
        }

        //Click on Task tar
        public bool ClickOnTaskorIncident(bool isdailyReport)
        {
            if (isdailyReport)
            {
                return taskLink.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Task link", masterSheetRow);
            }
            else
            {
                return incidentLink.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Incident link", masterSheetRow);
            }
        }

        //Click on My Teams Open Tasks from My Teams Open Tasks dropdown
        public bool SelectMyOpenTask(bool isBreachedReport, bool isdailyReport)
        {
            myOpenTask.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "My open task", masterSheetRow);
            Thread.Sleep(3000);
            if (isdailyReport)
            {
                return myTeamTasksValue.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "My Team tasks", masterSheetRow);
            }
            else if(isBreachedReport)
            {
                return breachedTicketsForMyTeam.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Breached Tickets For My Team", masterSheetRow);
            }
            else
            {
                return openIncidentsActiveTasks.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Open Incidents with Active Tasks for My Teams", masterSheetRow);
            }
        }

        //Click on Export button
        public bool ClickOnExport(bool isdailyReport)
        {
            exportButton.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Export button", masterSheetRow);
            Thread.Sleep(2000);
            popupYesButton.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Yes button", masterSheetRow);
            Thread.Sleep(2000);
            if (isdailyReport)
            {
                popupYesButton.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Yes button", masterSheetRow);
            }
            Thread.Sleep(6000);
            return true;
        }

        //Click on status dropdown and apply filter Accepted, Assigned and Logged
        public void FilterStatus()
        {
            Thread.Sleep(5000);
            Actions action = new Actions(driver);
            action.MoveToElement(status).Build().Perform();
            statusDropdown.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "status dropdown", masterSheetRow);
            statusFilters.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "status filter", masterSheetRow);

            //Clear the filter if already selected any.
            clearFilterField.SendKeys(" ", ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Clear filter", masterSheetRow);
            clearFilterButton.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "checkbox", masterSheetRow);
            Thread.Sleep(4000);

            //Select Accepted, Assigned and Logged checkboxes
            acceptCkeckbox.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Accepted checkbox", masterSheetRow);
            assignedCkeckbox.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Assigned checkbox", masterSheetRow);
            loggedCkeckbox.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Logged checkbox", masterSheetRow);
            Thread.Sleep(5000);

            //Click on opened dropdown to close the dropdown
            Actions action1 = new Actions(driver);
            action1.Click(activeDropdown).Build().Perform();
        }

        //Click on parent Object Name dropdown and add filter as incident.
        public void FilterparentObjectName(bool isBreachedReport, bool isFutureReportFlag)
        {
            
            /*if (isBreachedReport)
            {
                //Enter 'Incident' value for ParentObject drop-down filter 
                Actions action = new Actions(driver);
                action.MoveToElement(parentObjectName).Build().Perform();
                parentObjectdropdown.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "ParentObject dropdown", masterSheetRow);
                filters.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "ParentObject filter dropdown", masterSheetRow);
                parentObjectFilterTextbox.SendKeys("Incident", ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Filter text box", masterSheetRow);
                Thread.Sleep(5000);
                action.Click(activeDropdown).Build().Perform();
                Thread.Sleep(3000);

                //Select 'true' value for Breached pass drop-down filter
                Actions action1 = new Actions(driver);
                action1.MoveToElement(breachedPassTimeName).Build().Perform();
                Thread.Sleep(2000);
                breachedPassDropdown.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Breached pass dropdown", masterSheetRow);
                action1.MoveToElement(filters).Build().Perform();

                if (!trueCheckBox.Selected)
                {
                    trueCheckBox.Click();
                    Thread.Sleep(3000);
                }
                if (falseCheckBox.Selected)
                {
                    falseCheckBox.Click();
                    Thread.Sleep(3000);
                }
                action1.Click(activeDropdown).Build().Perform();
            }*/
            if (isFutureReportFlag)
            {
                /*//select Assigned, In Progress, Logged, Received, Waiting for 3rd Party, Waiting for Change, Waiting for Problem Resolution for status drop-down column 
                Actions action = new Actions(driver);
                action.MoveToElement(status).Build().Perform();
                statusDropdown.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Status dropdown", masterSheetRow);
                action.MoveToElement(filters).Build().Perform();
                //Uncheck the checkUncheckCheckbox
                if (checkUncheckCheckbox.Selected)
                {
                    UncheckCheckbox.Click();
                    Thread.Sleep(3000);
                }
                assignedCheckbox.Click();
                Thread.Sleep(2000);
                inProgressCheckbox.Click();
                Thread.Sleep(2000);
                loggedCheckbox.Click();
                Thread.Sleep(2000);
                receivedCheckbox.Click();
                Thread.Sleep(2000);
                waitingFor3rdPartyCheckbox.Click();
                Thread.Sleep(2000);
                waitingForChangeCheckbox.Click();
                Thread.Sleep(2000);
                waitingForProblemResolutionCheckbox.Click();
                Thread.Sleep(2000);
                action.Click(activeDropdown).Build().Perform();
                Thread.Sleep(2000);*/

                //Select 'false' value for Breached pass drop-down filter
                Actions action2 = new Actions(driver);
                action2.MoveToElement(breachedPassTimeName).Build().Perform();
                Thread.Sleep(3000);
                breachedPassDropdown.Click(ServEssConstant.SERVESSFUTUREBREACHEPAGE, "Breachedpass dropdown", masterSheetRow);
                action2.MoveToElement(filters).Build().Perform();
                if (trueCheckBox.Selected)
                {
                    trueCheckBox.Click();
                    Thread.Sleep(3000);
                }
                if (!falseCheckBox.Selected)
                {
                    falseCheckBox.Click();
                    Thread.Sleep(3000);
                }
                action2.Click(activeDropdown).Build().Perform();
            }
        }
    }

}
