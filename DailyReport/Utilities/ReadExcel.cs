﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPWAutomation.Utilities
{
    class ReadExcel
    {
        string connection = ConfigurationManager.AppSettings["XlsxConn"].ToString();
        private DataTable PopulateDataTable(string filePath)
        {
           
            string connectionString = string.Format(connection, filePath);
            OleDbConnection connExcel = new OleDbConnection(connectionString);
            OleDbCommand cmdExcel = new OleDbCommand();

            DataTable dt = null;
            try
            {
                cmdExcel.Connection = connExcel;
                //Get the name of First Sheet
                string SheetName = GetSheetName(connExcel);

                //Read Data from Sheet
                dt = ReadExcelSheet(SheetName, cmdExcel, connExcel);
            }
            catch (OleDbException ex)
            {

                return null;
            }

            return dt;
        }
        private DataTable ReadExcelSheet(string sheetName, OleDbCommand cmdExcel, OleDbConnection connExcel)
        {

            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            try
            {
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();
            }
            catch (InvalidOperationException ex)
            {

                return null;
            }
            catch (OleDbException ex)
            {

                return null;
            }
            return dt;
        }

        private string GetSheetName(OleDbConnection connExcel)
        {
            try
            {
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();
                return SheetName;
            }
            catch (OleDbException ex)
            {
               
               
            }
            catch (InvalidOperationException ex)
            {
               
             
            }
            catch (ArgumentException ex)
            {
               
               
            }
            return string.Empty;
        }
    }
}
